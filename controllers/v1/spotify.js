// import modules
const SpotifyWebApi = require('spotify-web-api-node');

// import files, vars and others
const ErrorResponse = require('../../utils/errorResponse');
const asyncHandler = require('../../middleware/async');
const LIMIT = 50;

// load models
const User = require('../../models/User');

const spotifyCredentials = {
  clientId: process.env.SPOTIFY_CLIENT_ID,
  clientSecret: process.env.SPOTIFY_CLIENT_SECRET,
  redirectUri: process.env.HOST + process.env.SPOTIFY_REDIRECT_URI,
};

// @desc    authorization with spotify
// @route   GET /api/v1/auth/spotify
// @access  public
exports.spotifyAuthorization = asyncHandler(async (req, res, next) => {
  const spotifyApi = new SpotifyWebApi(spotifyCredentials);
  const scopes = [
    'user-read-email',
    'user-read-recently-played',
    'user-follow-read',
  ];

  // Create the authorization URL
  const authorizeURL = spotifyApi.createAuthorizeURL(
    scopes,
    process.env.JWT_SECRET,
  );

  res.redirect(authorizeURL);
});

// @desc    redirected by spotify api
// @route   GET /api/v1/auth/spotify/callback
// @access  public
exports.spotifyRedirect = asyncHandler(async (req, res, next) => {
  const spotifyApi = new SpotifyWebApi(spotifyCredentials);
  const authorizationCode = req.query.code || null;

  try {
    // Retrieve an access token and a refresh token.
    const data = await spotifyApi.authorizationCodeGrant(authorizationCode);
    if (data.statusCode === 200) {
      spotifyApi.setAccessToken(data.body['access_token']);
      spotifyApi.setRefreshToken(data.body['refresh_token']);

      // Retrieve the profile data of the spotify user.
      const spotifyUser = await spotifyApi.getMe();

      // Get artists list of 50 recently played tracks.
      let recentlyPlayedTracksArtists = [];
      const recentlyPlayedTracks = await spotifyApi.getMyRecentlyPlayedTracks({
        limit: LIMIT,
      });
      recentlyPlayedTracks.body.items.map(each =>
        each.track.artists.map(artist => {
          if (!recentlyPlayedTracksArtists.includes(artist.id))
            recentlyPlayedTracksArtists.push(artist.id);
        }),
      );

      // Get the music categories list from artists of 50 recently played tracks.
      let musicCategories = [];
      // In one request max 50 artists data can be retrieved, so limited the artists array to 50.
      const artists = await spotifyApi.getArtists(
        recentlyPlayedTracksArtists.slice(0, LIMIT),
      );
      artists.body.artists.map(each =>
        each.genres.map(category => {
          if (!musicCategories.includes(category))
            musicCategories.push(category);
        }),
      );

      // Find spotify user profile pic.
      const photo =
        spotifyUser.body.images.length > 0
          ? spotifyUser.body.images[0].url
          : 'no-user-photo.jpg';

      // Spotify user found in database.
      let user = await User.findOneAndUpdate(
        { email: spotifyUser.body.email },
        {
          email: spotifyUser.body.email,
          name: spotifyUser.body.display_name,
          musicPreferences: musicCategories,
          artistPreferences: recentlyPlayedTracksArtists,
          photo,
        },
        {
          new: true,
          runValidators: true,
        },
      );

      if (!user) {
        // Creating new user in database with spotifyId field which make make password field not-required.
        user = await User.create({
          verified: true,
          spotifyId: spotifyUser.body.id,
          email: spotifyUser.body.email,
          name: spotifyUser.body.display_name,
          musicPreferences: musicCategories,
          artistPreferences: recentlyPlayedTracksArtists,
          photo,
        });
      }

      sendTokenResponse(user, 200, res);
    }
  } catch (err) {
    return next(
      new ErrorResponse(
        'Something went wrong with spotify authentication.',
        400,
      ),
    );
  }
});

// Get token from model and send response
const sendTokenResponse = (user, statusCode, res) => {
  // create token
  const token = user.getSignedJwtToken();

  res.redirect(`${process.env.APP_HOST}/dashboard#${token}`);
};

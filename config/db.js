// import modules
const mongoose = require('mongoose');
const DB_URI =
  process.env.NODE_ENV === 'test'
    ? process.env.MONGO_URI_TEST
    : process.env.MONGO_URI;

const connectDB = async () => {
  const conn = await mongoose.connect(DB_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  });

  console.log(`mongodb connected: ${conn.connection.host}`.cyan.underline.bold);
};

module.exports = connectDB;

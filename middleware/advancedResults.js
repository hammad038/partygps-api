// import modules
// import files, vars and others
// load models

const advancedResults = (model, populate) => async (req, res, next) => {
  let query;

  // copy of user query
  const reqQuery = { ...req.query };

  // fields to exclude from user query
  const removeFields = ['select', 'sort', 'page', 'limit'];

  // deleting fields from request query
  removeFields.forEach(param => delete reqQuery[param]);

  // covert user query to string
  let queryStr = JSON.stringify(reqQuery);

  // create operators query
  queryStr = queryStr.replace(/\b(gt|gte|lt|lte|in)\b/g, match => `$${match}`);

  // finding resources with query
  query = model.find(JSON.parse(queryStr));

  // response only user selected fields
  if(req.query.select) {
    const fields = req.query.select.split(',').join(' ');
    query = query.select(fields);
  }

  // sorting response
  if(req.query.sort) {
    const sortBy = req.query.sort.split(',').join(' ');
    query = query.sort(sortBy);
  } else {
    query = query.sort('-createdAt');
  }

  // pagination
  const page = parseInt(req.query.page, 10) || 1;
  const limit = parseInt(req.query.limit, 10) || 10;
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  const total = await model.countDocuments();

  query = query.skip(startIndex).limit(limit);

  if(populate) {
    query = query.populate(populate);
  }

  // executing query
  const results = await query;

  // pagination results
  const pagination = {};
  if (endIndex < total) {
    pagination.next = {
      page: page + 1,
      limit
    };
  }

  if (startIndex > 0) {
    pagination.prev = {
      page: page - 1,
      limit
    };
  }

  res.advancedResults = {
    success: true,
    count: results.length,
    pagination,
    data: results,
  }

  next();
}

module.exports = advancedResults;

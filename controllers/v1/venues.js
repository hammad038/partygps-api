// import files, vars and others
const path = require('path');
const ErrorResponse = require('../../utils/errorResponse');
const asyncHandler = require('../../middleware/async');

// load models
const Venue = require('../../models/Venue');

// @desc    get all venues
// @route   GET /api/v1/venues/
// @access  public
exports.getVenues = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc    get venue with id
// @route   GET /api/v1/venues/:id
// @access  public
exports.getVenue = asyncHandler(async (req, res, next) => {
  const venue = await Venue.findById(req.params.id);

  if (!venue) {
    return next(
      new ErrorResponse(`Venue not found with id: ${req.params.id}`, 404),
    );
  }

  res.status(200).json({ success: true, data: venue });
});

// @desc    create new venue
// @route   POST /api/v1/venues/
// @access  private
exports.createVenue = asyncHandler(async (req, res, next) => {
  const venue = await Venue.create(req.body);

  res.status(201).json({ success: true, data: venue });
});

// @desc    update venue with id
// @route   PUT /api/v1/venues/:id
// @access  private
exports.updateVenue = asyncHandler(async (req, res, next) => {
  let venue = await Venue.findById(req.params.id);

  if (!venue) {
    return next(
      new ErrorResponse(`Venue not found with id: ${req.params.id}`, 404),
    );
  }

  venue = await Venue.findOneAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({ success: true, data: venue });
});

// @desc    delete venue with id
// @route   DELETE /api/v1/venues/:id
// @access  private
exports.deleteVenue = asyncHandler(async (req, res, next) => {
  const venue = await Venue.findById(req.params.id);

  if (!venue) {
    return next(
      new ErrorResponse(`Venue not found with id: ${req.params.id}`, 404),
    );
  }

  venue.remove();

  res.status(200).json({
    success: true,
    data: `venue with id ${req.params.id} deleted.`,
  });
});

// @desc    get venues in radius
// @route   GET /api/v1/venues/radius/:lat/:lng/:distance/:unit
// @access  public
exports.getVenuesInRadius = asyncHandler(async (req, res, next) => {
  const { lat, lng, distance, unit } = req.params;

  // calculate earth radius (distance/radius of earth)
  // radius of earth = 6378.1 km = 3963 mi
  const earthRadius = unit !== 'mi' ? 6378.1 : 3963;
  const radius = distance / earthRadius;

  const venues = await Venue.find({
    location: { $geoWithin: { $centerSphere: [[lng, lat], radius] } },
  });

  res.status(200).json({ success: true, count: venues.length, data: venues });
});

// @desc    upload photo for venue
// @route   PUT /api/v1/venues/:id/photo
// @access  private
exports.uploadVenuePhoto = asyncHandler(async (req, res, next) => {
  const venue = await Venue.findById(req.params.id);

  if (!venue) {
    return next(
      new ErrorResponse(`Venue not found with id: ${req.params.id}`, 404),
    );
  }

  // file upload validation
  if (!req.files) {
    return next(new ErrorResponse('Please upload a file.', 404));
  }

  const file = req.files.file;

  // upload file type (i.e. image) validation
  if (!file.mimetype.startsWith('image')) {
    return next(new ErrorResponse('Please upload an image file.', 400));
  }

  // upload file size validation
  if (file.size > process.env.FILE_MAX_SIZE) {
    return next(
      new ErrorResponse(
        `File size should be less than ${process.env.FILE_MAX_SIZE /
          1000000} MB.`,
        400,
      ),
    );
  }

  // create custom filename
  file.name = `venue_${venue._id}${path.parse(file.name).ext}`;

  // upload the file
  file.mv(`${process.env.FILE_UPLOAD_PATH}/venue/${file.name}`, async err => {
    if (err) {
      console.log(err);
      return next(new ErrorResponse('File upload failed.', 500));
    }

    // find the photo name in the database
    await Venue.findByIdAndUpdate(req.params.id, {
      photo: `${process.env.HOST}/uploads/venue/${file.name}`,
    });

    res.status(200).json({
      success: true,
      data: `file ${file.name} uploaded successfully.`,
    });
  });
});

// import modules
const express = require('express');
const router = express.Router();

// import files, vars and others
const {
  spotifyAuthorization,
  spotifyRedirect,
} = require('../../../controllers/v1/spotify');

router.get('/', spotifyAuthorization);
router.get('/callback', spotifyRedirect);

module.exports = router;

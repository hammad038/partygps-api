// Import modules.
const app = require('../server');
const connectDb = require('../config/db');
const supertest = require('supertest');
const mongoose = require('mongoose');
const request = supertest(app);

// Load variables and others.
const {
  HOST,
  PORT,
  MONGO_URI_TEST,
  TEST_USER_NAME,
  TEST_USER_EMAIL,
  TEST_USER_PASSWORD,
} = process.env;
const colectionsDropOrDelete = require('../utils/colectionsDropOrDelete');
let token;

// Load models.
const User = require('../models/User');

// Runs before all tests.
beforeAll(async () => {
  // Database connection.
  connectDb();
});

// Runs after all tests.
afterAll(async () => {
  // const collectionKeysList = Object.keys(mongoose.connection.collections);

  // Deleting content in users collection.
  await colectionsDropOrDelete(['users'], 'delete');
});

// Register new user.
it('/api/v1/auth/register', async done => {
  // Creating a new user in database.
  const registeredUser = await request.post('/api/v1/auth/register').send({
    name: TEST_USER_NAME,
    email: TEST_USER_EMAIL,
    password: TEST_USER_PASSWORD,
  });

  expect(registeredUser.status).toBe(201);
  expect(registeredUser.body.success).toBe(true);
  expect(registeredUser.body.token).not.toBeUndefined();

  // Check if new user successfully created in database.
  const user = await User.findOne({ email: TEST_USER_EMAIL });
  expect(user.verified).toBe(false);
  expect(user.status).toBe(true);
  expect(user.name).not.toBe('');
  expect(user.name).not.toBeUndefined();

  done();
});

// Login user.
it('/api/v1/auth/login', async done => {
  const loginUser = await request.post('/api/v1/auth/login').send({
    email: TEST_USER_EMAIL,
    password: TEST_USER_PASSWORD,
  });

  expect(loginUser.status).toBe(200);
  expect(loginUser.body.success).toBe(true);
  expect(loginUser.body.token).not.toBeUndefined();

  // Setting token in global scope for other endpoint in the current file.
  token = loginUser.body.token;

  done();
});

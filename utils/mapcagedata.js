const axios  = require('axios');

const formattedAddress = async coordinates => {
    try {
        const response = await axios.get(`https://api.opencagedata.com/geocode/v1/json?key=${process.env.OPEN_CAGE_DATA_API_KEY}&q=${coordinates[1]},${coordinates[0]}`);
        return response.data.results[0].formatted;
    } catch (err) {
        console.error(err);
    }
}

module.exports = formattedAddress;
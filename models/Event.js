// import modules
const mongoose = require('mongoose');

const EventSchema = new mongoose.Schema({
  createdAt: {
    type: Date,
    default: Date.now,
  },

  venue: {
    type: mongoose.Schema.ObjectId,
    ref: 'venue',
    required: true,
  },

  // @TODO make promoter field required for production, for seeder it should not be required.
  promoter: {
    type: mongoose.Schema.ObjectId,
    ref: 'user',
    // required: true,
  },

  // @TODO event name should not be unique but before creating new event find in
  //       events collection that an event that have same name and venue ObjectId
  //       can't be created.
  name: {
    type: String,
    required: [true, 'Venue name is required.'],
    trim: true,
    unique: true,
    maxlength: [150, 'Name should be maximum 150 ch long.'],
  },

  description: {
    type: String,
    required: true,
    trim: true,
    minlength: [100, 'Description should be at-least 100 ch long.'],
  },

  startDate: {
    type: Date,
    required: [true, 'Event start date is required.'],
  },

  startTime: {
    type: Date,
    required: [true, 'Event start time is required.'],
  },

  endDate: {
    type: Date,
    default: null,
  },

  endTime: {
    type: Date,
    default: null,
  },

  tags: {
    type: [String],
    default: [],
  },

  website: {
    type: String,
    match: [
      /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
      'Please use a valid URL with HTTP or HTTPS.',
    ],
  },

  promoVideo: {
    type: String,
    match: [
      /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
      'Please use a valid URL with HTTP or HTTPS.',
    ],
  },

  published: {
    type: Boolean,
    default: false,
  },

  photo: {
    type: String,
    default: `${process.env.HOST}/uploads/event/no-event-photo.jpg`,
  },

  artists: {
    type: [String],
  },

  others: {
    freeEntry: {
      type: Boolean,
    },

    dresscode: {
      type: String,
      trim: true,
      maxlength: [150, 'Name should be maximum 150 ch long.'],
    },
  },
});

module.exports = mongoose.model('event', EventSchema);

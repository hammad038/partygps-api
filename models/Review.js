// import modules
const mongoose = require('mongoose');

const ReviewSchema = new mongoose.Schema({
  createdAt: {
    type: Date,
    default: Date.now,
  },

  venue: {
    type: mongoose.Schema.ObjectId,
    ref: 'venue',
    required: true,
  },

  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'user',
    required: true,
  },

  text: {
    type: String,
    required: [true, 'Review field is required.'],
    trim: true,
    maxlength: [600, 'Review should be maximum 600 ch long.'],
  },

  rating: {
    type: Number,
    required: [true, 'Review rating is required.'],
    trim: true,
    min: 1,
    max: 10,
  },
});

// @TODO each user can add max 1 review per venue. The feature is not in working state.
// ReviewSchema.index({ Venue: 1, User: 1 }, { unique: true });

// get avg rating and save
ReviewSchema.statics.getAverageRating = async function(venueId) {
  const obj = await this.aggregate([
    {
      $match: { venue: venueId }
    },
    {
      $group: {
        _id: '$venue',
        averageRating: { $avg: '$rating' }
      }
    }
  ]);

  try {
    await this.model('venue').findByIdAndUpdate(venueId, {
      averageRating: obj[0].averageRating
    });
  } catch (err) {
    console.error(err);
  }
};

// call getAverageRating after save
ReviewSchema.post('save', function() {
  this.constructor.getAverageRating(this.venue);
});

// call getAverageRating before remove
ReviewSchema.pre('remove', function() {
  this.constructor.getAverageRating(this.venue);
});

module.exports = mongoose.model('review', ReviewSchema);

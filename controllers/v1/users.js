// import modules
const crypto = require('crypto');

// import files, vars and others
const ErrorResponse = require('../../utils/errorResponse');
const asyncHandler = require('../../middleware/async');

// load models
const User = require('../../models/User');

// @desc    get all users
// @route   GET /api/v1/users
// @access  private/admin
exports.getUsers = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc    get user
// @route   GET /api/v1/users/:id
// @access  private/admin
exports.getUser = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.params.id);

  if(!user) {
    return next(new ErrorResponse(`User not found with id ${req.params.id}.`, 400));
  }

  res.status(200).json({ success: true, data: user });
});

// @desc    create new user
// @route   POST /api/v1/users
// @access  private/admin
exports.createUser = asyncHandler(async (req, res, next) => {
  const user = await User.create(req.body);

  res.status(201).json({ success: true, data: user });
});

// @desc    update user
// @route   PUT /api/v1/users/:id
// @access  private/admin
exports.updateUser = asyncHandler(async (req, res, next) => {
  let user = await User.findById(req.params.id);

  if (!user) {
    return next(
      new ErrorResponse(`User not found with id: ${req.params.id}`, 404)
    );
  }

  user = await User.findOneAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  res.status(200).json({ success: true, data: user });
});

// @desc    delete user
// @route   DELETE /api/v1/users/:id
// @access  private/admin
exports.deleteUser = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.params.id);

  if (!user) {
    return next(new ErrorResponse(`User not found with id: ${req.params.id}`, 404));
  }

  user.remove();

  res.status(200).json({
    success: true,
    data: `User with id ${req.params.id} deleted.`
  });
});

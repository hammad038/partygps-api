const mongoose = require('mongoose');

module.exports = colectionsDropOrDelete = async (
  collectionKeysList,
  action,
) => {
  for (const collectionKey of collectionKeysList) {
    // Making each collection empty.
    const collection = mongoose.connection.collections[collectionKey];

    try {
      action === 'drop'
        ? await collection.drop()
        : await collection.deleteMany();
    } catch (error) {
      if (error.message === 'ns not found') return;

      console.log(error.message);
    }
  }
};

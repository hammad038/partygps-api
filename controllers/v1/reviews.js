// import modules

// import files, vars and others
const ErrorResponse = require('../../utils/errorResponse');
const asyncHandler = require('../../middleware/async');

// load models
const Venue = require('../../models/Venue');
const Review = require('../../models/Review');

// @desc    get reviews for venue with id
// @route   GET /api/v1/reviews/
// @route   GET /api/v1/venues/:venueId/reviews
// @access  public
exports.getReviewsForVenue = asyncHandler(async (req, res, next) => {
  if(req.params.venueId) {
    const reviews = await Review.find({ venue: req.params.venueId });

    return res.status(200).json({
      success: true,
      count: reviews.length,
      data: reviews,
    });
  } else {
    res.status(200).json(res.advancedResults);
  }
});

// @desc    get a review
// @route   GET /api/v1/reviews/:id
// @access  public
exports.getReview = asyncHandler(async (req, res, next) => {
  const review = await Review.findById(req.params.id).populate({
    path: 'venue',
    select: 'name location',
  });

  if(!review) {
    return next(new ErrorResponse(`Review not found with id: ${req.params.id}`, 404));
  }

  res.status(200).json({ success: true, data: review });
});

// @desc    add a review
// @route   POST /api/v1/venues/:venueId/reviews
// @access  private
exports.addReview = asyncHandler(async (req, res, next) => {
  req.body.venue = req.params.venueId;
  req.body.user = req.user.id;

  const venue = await Venue.findById(req.params.venueId);

  if(!venue) {
    return next(new ErrorResponse(`Venue not found with id: ${req.params.venueId}`, 404));
  }

  const review = await Review.create(req.body);

  res.status(201).json({ success: true, data: review });
});

// @desc      update review
// @route     PUT /api/v1/reviews/:id
// @access    private
exports.updateReview = asyncHandler(async (req, res, next) => {
  let review = await Review.findById(req.params.id);

  if (!review) {
    return next(
      new ErrorResponse(`Review not found with id: ${req.params.id}`, 404)
    );
  }

  // review belongs to user or user is admin
  if (review.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(new ErrorResponse(`Not authorized to update review`, 401));
  }

  review = await Review.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  res.status(200).json({
    success: true,
    data: review
  });
});

// @desc      delete review
// @route     DELETE /api/v1/reviews/:id
// @access    private
exports.deleteReview = asyncHandler(async (req, res, next) => {
  const review = await Review.findById(req.params.id);

  if (!review) {
    return next(
      new ErrorResponse(`Review not found with id: ${req.params.id}`, 404)
    );
  }

  // review belongs to user or user is admin
  if (review.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(new ErrorResponse(`Not authorized to update review`, 401));
  }

  await review.remove();

  res.status(200).json({ success: true, data: `Review deleted successfully with id: ${req.params.id}` });
});

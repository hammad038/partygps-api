// import modules
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const UserSchema = new mongoose.Schema({
  createdAt: {
    type: Date,
    default: Date.now,
  },

  name: {
    type: String,
    required: [true, 'Name is required.'],
    trim: true,
    maxlength: [150, 'Name should be maximum 150 ch long.'],
  },

  email: {
    type: String,
    required: [true, 'Email is required.'],
    trim: true,
    lowercase: true,
    unique: true,
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      'Please add a valid email.',
    ],
  },

  /*
   * if user login with spotify, spotifyId field will be filled and password field will not be required to create a
   * new user in database.
   * */
  password: {
    type: String,
    select: false,
    required: [
      function() {
        return this.spotifyId ? false : true;
      },
      'Password is required.',
    ],
    minlength: [6, 'Password should be minimum 6 ch long.'],
  },

  spotifyId: String,

  resetPasswordToken: String,

  resetPasswordExpire: Date,

  verified: {
    type: Boolean,
    default: false,
  },

  status: {
    type: Boolean,
    default: true,
  },

  verifyAccountToken: String,

  role: {
    type: String,
    enum: ['admin', 'moderator', 'promoter', 'guest'],
    default: 'guest',
  },

  musicPreferences: {
    type: [String],
  },

  artistPreferences: {
    type: [String],
  },

  lastLogin: Date,

  phone: {
    type: String,
    maxlength: [20, 'Phone number can not be longer than 20 ch.'],
  },

  photo: {
    type: String,
    default: `${process.env.HOST}/uploads/user/no-user-photo.jpg`,
  },
});

// hash the password using bcrpytjs
UserSchema.pre('save', async function(next) {
  // if the password field is not modified then do not create password hash
  if (!this.isModified('password')) {
    next();
  }
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

// sign jwt token
UserSchema.methods.getSignedJwtToken = function() {
  const payload = {
    id: this._id,
    name: this.name,
    email: this.email,
    photo: this.photo,
    role: this.role,
    verified: this.verified,
    status: this.status,
    musicPreferences: this.musicPreferences,
    artistPreferences: this.artistPreferences,
  };
  return jwt.sign(payload, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRE,
  });
};

// match the user entered password with hashed password in database
UserSchema.methods.matchPassword = async function(enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password);
};

// generate and hash forgot password token
UserSchema.methods.getResetPasswordToken = function() {
  // generate token
  const resetToken = crypto.randomBytes(20).toString('hex');

  // hash token and set to resetPasswordToken field
  this.resetPasswordToken = crypto
    .createHash('sha256')
    .update(resetToken)
    .digest('hex');

  // set resetPasswordExpire field
  this.resetPasswordExpire = Date.now() + 10 * 60 * 100;

  return resetToken;
};

module.exports = mongoose.model('user', UserSchema);

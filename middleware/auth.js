// import modules
const jwt = require('jsonwebtoken');

// import files, vars and others
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');

// load models
const User = require('../models/User');

// protect routes
exports.protect = asyncHandler(async (req, res, next) => {
  let token;
  if(req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
    token = req.headers.authorization.split(' ')[1];
  }

  // token exist
  if(!token) {
    return next(new ErrorResponse('Not authorized to access this route.', 401));
  }

  try {
    // verify token
    const decode = jwt.verify(token, process.env.JWT_SECRET);
    req.user = await User.findById(decode.id);
    next();
  } catch (err) {
    return next(new ErrorResponse('Not authorized to access this route.', 401));
  }
});

// grant access to specific roles
exports.authorize = (...roles) => {
  return (req, res, next) => {
    if(!roles.includes(req.user.role)) {
      return next(new ErrorResponse(`'${req.user.role}' user role is not authorized to perform this action.`,
        401));
    }

    next();
  }
}

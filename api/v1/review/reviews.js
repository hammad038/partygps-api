// import modules
const express = require('express');
const router = express.Router({ mergeParams: true });

// import files, vars and others
const {
  getReviewsForVenue,
  getReview,
  addReview,
  updateReview,
  deleteReview,
} = require('../../../controllers/v1/reviews');
const { protect, authorize } = require('../../../middleware/auth');
const advancedResults = require('../../../middleware/advancedResults');

// load models
const Review = require('../../../models/Review');

router
  .route('/')
  .get(
    advancedResults(Review, {
      path: 'venue',
      select: 'name location',
    }),
    getReviewsForVenue,
  )
  .post(protect, authorize('admin', 'guest'), addReview);

router
  .route('/:id')
  .get(getReview)
  .put(protect, authorize('admin', 'guest'), updateReview)
  .delete(protect, authorize('admin', 'guest'), deleteReview);

module.exports = router;

// import modules
const express = require('express');
const router = express.Router();

// import files, vars and others
const {
  getVenues,
  getVenue,
  createVenue,
  updateVenue,
  deleteVenue,
  getVenuesInRadius,
  uploadVenuePhoto
} = require('../../../controllers/v1/venues');
const { protect, authorize } = require('../../../middleware/auth');
const advancedResults = require('../../../middleware/advancedResults');

// load models
const Venue = require('../../../models/Venue');

// include other resource routers
const eventsRouter = require('../event/events');
const reviewsRouter = require('../review/reviews');

// re-route into other resource routers
router.use('/:venueId/events', eventsRouter);
router.use('/:venueId/reviews', reviewsRouter);

router
  .route('/')
  .get(advancedResults(Venue, 'events'),getVenues)
  .post(protect, authorize('admin', 'moderator'), createVenue);

router
  .route('/:id')
  .get(getVenue)
  .put(protect, authorize('admin', 'moderator'), updateVenue)
  .delete(protect, authorize('admin'), deleteVenue);

router
  .route('/:id/photo')
  .put(protect, authorize('admin', 'moderator'), uploadVenuePhoto);

router
  .route('/radius/:lat/:lng/:distance/:unit')
  .get(getVenuesInRadius);

module.exports = router;

// import modules
const mongoose = require('mongoose');
const slugify = require('slugify');
const formattedAddress = require('../utils/mapcagedata');

const VenueSchema = new mongoose.Schema(
  {
    createdAt: {
      type: Date,
      default: Date.now,
    },

    name: {
      type: String,
      required: [true, 'Venue name is required.'],
      trim: true,
      unique: true,
      maxlength: [100, 'Name should be maximum 100 ch long.'],
    },

    slug: {
      type: String,
      trim: true,
    },

    description: {
      type: String,
      // required: true,
      trim: true,
      // minlength: [100, 'Description should be at-least 100 ch long.'],
    },

    category: {
      type: [String],
      required: [true, 'Venue category is required.'],
      default: [],
      enum: ['bar', 'cafe', 'nightclub', 'pub'],
    },

    tags: {
      type: [String],
      default: [],
    },

    location: {
      type: {
        type: String,
        enum: ['Point'],
      },
      coordinates: {
        type: [Number],
        index: '2dsphere',
        required: [true, 'Coordinates are required.'],
        trim: true,
      },
      street: {
        type: String,
        trim: true,
      },
      housenumber: {
        type: String,
        trim: true,
      },
      zipcode: {
        type: Number,
        trim: true,
      },
      city: {
        type: String,
        trim: true,
      },
      country: {
        type: String,
        trim: true,
      },
      countryCode: {
        type: String,
        trim: true,
      },
    },

    formattedAddress: {
      type: String,
      trim: true,
    },

    website: {
      type: String,
      match: [
        /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
        'Please use a valid URL with HTTP or HTTPS.',
      ],
    },

    phone: {
      type: String,
      maxlength: [20, 'Phone number can not be longer than 20 ch.'],
    },

    email: {
      type: String,
      match: [
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        'Please add a valid email.',
      ],
    },

    socialMedia: {
      facebook: {
        type: String,
        match: [
          /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
          'Please use a valid URL with HTTP or HTTPS.',
        ],
      },
      instagram: {
        type: String,
        match: [
          /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
          'Please use a valid URL with HTTP or HTTPS.',
        ],
      },
      twitter: {
        type: String,
        match: [
          /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
          'Please use a valid URL with HTTP or HTTPS.',
        ],
      },
      youtube: {
        type: String,
        match: [
          /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
          'Please use a valid URL with HTTP or HTTPS.',
        ],
      },
    },

    averageRating: {
      type: Number,
      min: [1, 'Rating must be at least 1.'],
      max: [10, 'Rating must not be more than 10.'],
    },

    published: {
      type: Boolean,
      default: false,
    },

    photo: {
      type: String,
      default: `${process.env.HOST}/uploads/venue/no-venue-photo.jpg`,
    },

    others: {
      wheelChair: {
        type: Boolean,
      },
      toiletsWheelChair: {
        type: Boolean,
      },
      outdoorSeating: {
        type: Boolean,
      },
      openingHours: {
        type: String,
      },
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  },
);

// create venue slug from the name
VenueSchema.pre('save', function(next) {
  this.slug = slugify(this.name, { lower: true });
  next();
});

// mapcagedata api to create formatted address
VenueSchema.pre('save', async function(next) {
  const { coordinates } = this.location;
  this.formattedAddress = await formattedAddress(coordinates);
  next();
});

// delete events when venue is deleted
VenueSchema.pre('remove', async function(next) {
  console.log(`deleting venue '${this.name}' and the events of this venue.`);
  await this.model('event').deleteMany({ venue: this._id });
  next();
});

// reverse populate with virtuals
VenueSchema.virtual('events', {
  ref: 'event',
  localField: '_id',
  foreignField: 'venue',
  justOne: false,
});

module.exports = mongoose.model('venue', VenueSchema);

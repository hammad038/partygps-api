// import modules
const express = require('express');
const path = require('path');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const fileUpload = require('express-fileupload');

// load env variables
dotenv.config({ path: './config/.env' });

// import files, vars and others
const PORT = process.env.PORT || 5000;
const errorHandler = require('./middleware/error');
const connectDB = require('./config/db');
const venues = require('./api/v1/venue/venues');
const events = require('./api/v1/event/events');
const auth = require('./api/v1/auth/auth');
const spotify = require('./api/v1/auth/spotify');
const users = require('./api/v1/user/users');
const reviews = require('./api/v1/review/reviews');

// initialize express app
const app = express();

// express body parser middleware
app.use(express.json());

// logger middleware for development mode
if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'staging') {
  app.use(morgan('dev'));
}

// file upload middleware
app.use(fileUpload());

// set static assets folder
app.use(express.static(path.join(__dirname, 'public')));

// api routes
app.use('/api/v1/venues', venues);
app.use('/api/v1/events', events);
app.use('/api/v1/auth', auth);
app.use('/api/v1/auth/spotify', spotify);
app.use('/api/v1/users', users);
app.use('/api/v1/reviews', reviews);

// error handler
app.use(errorHandler);

// check if its test ENV
let server;
if (process.env.NODE_ENV !== 'test') {
  // connect database
  connectDB();

  // start node server
  server = app.listen(PORT, () => {
    console.log(
      `Server running in ${process.env.NODE_ENV} environment on port ${PORT}.`
        .yellow.bold,
    );
  });
}

// handle unhandled rejections
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red);
  // close server and exit.
  server.close(() => process.exit(1));
});

module.exports = app;

// import modules
const express = require('express');
const router = express.Router();

// import files, vars and others
const {
  register,
  login,
  getMe,
  updateUser,
  forgotPassword,
  resetPassword,
  updatePassword,
} = require('../../../controllers/v1/auth');
const { protect, authorize } = require('../../../middleware/auth');

router.post('/register', register);
router.post('/login', login);
router.get(
  '/me',
  protect,
  authorize('admin', 'moderator', 'promoter', 'guest'),
  getMe,
);
router.put(
  '/updateuser',
  protect,
  authorize('admin', 'moderator', 'promoter', 'guest'),
  updateUser,
);
router.put(
  '/updatepassword',
  protect,
  authorize('admin', 'moderator', 'promoter', 'guest'),
  updatePassword,
);
router.post('/forgotpassword', forgotPassword);
router.post('/resetpassword/:resettoken', resetPassword);

module.exports = router;

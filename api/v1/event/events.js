// import modules
const express = require('express');
const router = express.Router({ mergeParams: true });

// import files, vars and others
const {
  getEvents,
  getEvent,
  createEvent,
  updateEvent,
  deleteEvent,
  uploadEventPhoto
} = require('../../../controllers/v1/events');
const { protect, authorize } = require('../../../middleware/auth');
const advancedResults = require('../../../middleware/advancedResults');

// load models
const Event = require('../../../models/Event');

router
  .route('/')
  .get(advancedResults(Event, {
    path: 'venue',
    select: 'name location'
  }), getEvents)
  .post(protect, authorize('admin', 'moderator', 'promoter'), createEvent);

router
  .route('/:id')
  .get(getEvent)
  .put(protect, authorize('admin', 'moderator', 'promoter'), updateEvent)
  .delete(protect, authorize('admin'), deleteEvent);

router
  .route('/:id/photo')
  .put(protect, authorize('admin', 'moderator', 'promoter'), uploadEventPhoto);

module.exports = router;

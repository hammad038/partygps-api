// import files, vars and others
const path = require('path');
const ErrorResponse = require('../../utils/errorResponse');
const asyncHandler = require('../../middleware/async');

// load models
const Venue = require('../../models/Venue');
const Event = require('../../models/Event');

// @desc    get events
// @route   GET /api/v1/events/
// @route   GET /api/v1/venues/:venueId/events
// @access  public
exports.getEvents = asyncHandler(async (req, res, next) => {
  if(req.params.venueId) {
    const events = await Event.find({ venue: req.params.venueId });

    return res.status(200).json({
      success: true,
      count: events.length,
      data: events,
    });
  } else {
    res.status(200).json(res.advancedResults);
  }
});

// @desc    get event with id
// @route   GET /api/v1/events/:id
// @access  public
exports.getEvent = asyncHandler(async (req, res, next) => {
  const event = await Event.findById(req.params.id).populate({
    path: 'venue',
    select: 'name location',
  });

  if (!event) {
    return next(new ErrorResponse(`Event not found with id: ${req.params.id}`, 404));
  }

  res.status(200).json({success: true, data: event});
});

// @desc    create new event
// @route   POST /api/v1/venues/:venueId/events/
// @access  private
exports.createEvent = asyncHandler(async (req, res, next) => {
  req.body.venue = req.params.venueId;
  req.body.promoter = req.user.id;

  const venue = await Venue.findById(req.params.venueId);

  if(!venue) {
    return next(new ErrorResponse(`No venue exist with this id: ${req.params.id} to add an event.`, 404));
  }

  const event = await Event.create(req.body);

  res.status(201).json({success: true, data: event});
});

// @desc    update event with id
// @route   PUT /api/v1/events/:id
// @access  private
exports.updateEvent = asyncHandler(async (req, res, next) => {
  let event = await Event.findById(req.params.id);

  if (!event) {
    return next(
      new ErrorResponse(`Event not found with id: ${req.params.id}`, 404)
    );
  }

  event = await Event.findOneAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  res.status(200).json({ success: true, data: event });
});

// @desc    delete event with id
// @route   DELETE /api/v1/events/:id
// @access  private
exports.deleteEvent = asyncHandler(async (req, res, next) => {
  const event = await Event.findById(req.params.id);

  if (!event) {
    return next(new ErrorResponse(`Event not found with id: ${req.params.id}`, 404));
  }

  event.remove();

  res.status(200).json({
    success: true,
    data: `Event with id ${req.params.id} deleted.`
  });
});

// @desc    upload photo for event
// @route   PUT /api/v1/events/:id/photo
// @access  private
exports.uploadEventPhoto = asyncHandler(async (req, res, next) => {
  const event = await Event.findById(req.params.id);

  if (!event) {
    return next(new ErrorResponse(`Event not found with id: ${req.params.id}`, 404));
  }

  // file upload validation
  if (!req.files) {
    return next(new ErrorResponse('Please upload a file.', 404));
  }

  const file = req.files.file;

  // upload file type (i.e. image) validation
  if(!file.mimetype.startsWith('image')) {
    return next(new ErrorResponse('Please upload an image file.', 400));
  }

  // upload file size validation
  if(file.size > process.env.FILE_MAX_SIZE) {
    return next(new ErrorResponse(`File size should be less than ${process.env.FILE_MAX_SIZE / 1000000} MB.`, 400));
  }

  // create custom filename
  file.name = `event_${event._id}${path.parse(file.name).ext}`;

  // upload the file
  file.mv(`${process.env.FILE_UPLOAD_PATH}/event/${file.name}`, async err => {
    if(err) {
      return next(new ErrorResponse('File upload failed.', 500));
    }

    // find the photo name in the database
    await Event.findByIdAndUpdate(req.params.id, { photo: file.name });

    res.status(200).json({ success: true, data: `file ${file.name} uploaded successfully.` });
  });
});

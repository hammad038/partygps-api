// import files, vars and others
const ErrorResponse = require('../utils/errorResponse');

const errorHandler = (err, req, res, next) => {
  let error = { ...err };
  error.message = err.message;

  // cast error or mongoose bad object _id
  if(err.name === 'CastError') {
    const message = `Resource not found with id: ${err.value}`;
    error = new ErrorResponse(message, '404');
  }

  // mongoose duplicate key error
  if(err.code === 11000) {
    const message = 'Field value is not unique';
    error = new ErrorResponse(message, '400');
  }

  // mongoose validation error
  if(err.name === 'ValidationError') {
    const message = Object.values(err.errors).map(eachError => eachError.message);
    error = new ErrorResponse(message, '400');
  }

  res.status(error.statusCode || 500).json({
    success: false,
    error: error.message || 'Server error.',
  })
}

module.exports = errorHandler;
